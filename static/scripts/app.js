document.addEventListener('DOMContentLoaded', () => {
  var channelMessages = [];
  var curPosition = 0;
  var emoji_keys = [
    ":)", "^-^",":D","|D",":'D",";)",";]","<3",":*",":P", ";P",":|",":/",":(",":o"
  ];
  var emoji_icons = [
    "😀","😁","😃","😅","😉","😏","😍","😘","😛","😜","😐","😞","😕","😮"
  ];

  var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);
  socket.on('connect', () => {
    if (!localStorage.getItem('nickname')) {
      showDialog("👤 Choose a nickname to enter", "Choose a nickname", "Enter the chat!", false, nicknameAction);
    } else {
      updateUsers();
      updateChannels();
      if(localStorage.getItem('activeChannel') !== null) {
        channelMessages = messages[localStorage.getItem('activeChannel')];
        document.querySelector("#write-message-input").disabled = false;
        document.querySelector(".emoji").disabled = false;
        document.querySelector(".send").disabled = false;
        selectChannel(localStorage.getItem('activeChannel'));
        updateMessages();
      }
      showButtons();

      socket.emit('user login', {'nickname': localStorage.getItem('nickname')});
      activateWriter();
    }
  });

  socket.on('users', data => {
    users = data;
    updateUsers();
    updateChannels();
  });

  socket.on('channels', data => {
    channels = data;
    updateChannels();
  });

  socket.on('messages', data => {
    messages = data;
    updateMessages();
  });

  nicknameAction = () => {
    const nick = document.querySelector('#modal-input').value.replace(/\s+/g, '');
    if(nick === "") {
      document.querySelector('#modal-error').innerHTML = "Plaese, choose a nickname."
      document.querySelector('#modal-error').style.display = "block";
    } else if(users.includes(nick)) {
      document.querySelector('#modal-error').innerHTML = "Nickname is taken."
      document.querySelector('#modal-error').style.display = "block";
    } else {
      const nickname = document.querySelector('#modal-input').value;
      localStorage.setItem('nickname', nickname);
      socket.emit('user login', {'nickname': nickname});
      hideModal();
      showButtons();
    }
  }

  updateUsers = () => {
    document.querySelector('#user-list').innerHTML = "";
    users.forEach(user => {
      const userItem = document.createElement("LI");
      if(user === localStorage.getItem('nickname')) {
        userItem.classList.add('me');
      }
      userItem.innerHTML = "👤 " + user;
      document.querySelector('#user-list').appendChild(userItem);
    });
  }

  newChannelAction = () => {
    const channel = document.querySelector("#modal-input").value;

    if(channel === "") {
      document.querySelector('#modal-error').innerHTML = "Set a channel name."
      document.querySelector('#modal-error').style.display = "block";
      hideModal();
    } else if(Object.keys(channels).includes(channel)) {
      document.querySelector('#modal-error').innerHTML = "Channel name is taken."
      document.querySelector('#modal-error').style.display = "block";
    } else {
      socket.emit('new channel', {'channel': channel, 'owner': localStorage.getItem('nickname')});
      hideModal();
      localStorage.setItem("mychannel", channel);
      localStorage.setItem("activeChannel", channel);
      document.querySelector("#messages-bg-text").innerHTML = "📡 " + channel;
      document.querySelector("#write-message-input").disabled = false;
      document.querySelector(".emoji").disabled = false;
      document.querySelector(".send").disabled = false;
      activateWriter();
      updateMessages();
      showButtons();
    }
  }

  sendAction = () => {
    let messageText = document.querySelector('#write-message-input').value;
    if(messageText.replace(/\s+/g, '') !== "") {
      const timestamp = new Date();
      emoji_keys.forEach(function(ekeys, index) {
        messageText = messageText.replace(ekeys, emoji_icons[index]);
      });
      socket.emit('message sent', {
        'channel': localStorage.getItem('activeChannel'),
        'message': {
          'nickname': localStorage.getItem('nickname'),
          'timestamp': timestamp.getTime(),
          'message_text': escape(messageText)
        }
      });
      document.querySelector("#write-message-input").value = "";
    }
  }

  activateWriter = () => {
    emoji_icons.forEach(emoji => {
      const emojiItem = document.createElement("DIV");
      emojiItem.innerHTML = emoji;
      emojiItem.onclick = function() {
        const tempMessage = document.querySelector("#write-message-input").value;
        const messagePre = tempMessage.substring(0, curPosition);
        const messagePost = tempMessage.substring(curPosition);
        document.querySelector("#write-message-input").value = messagePre + this.innerHTML + messagePost;
        document.querySelector(".insert-emoji").classList.remove("open");
        document.querySelector(".insert-emoji").classList.add("close");
        document.querySelector("#write-message-input").focus();
      }

      document.querySelector('.insert-emoji').appendChild(emojiItem);
    });

    document.querySelector("#write-message-input").onblur = function() {
      curPosition = this.selectionStart;
    }

    document.querySelector(".emoji").onclick = () => {
      if(document.querySelector(".insert-emoji").classList.contains("open")) {
        document.querySelector(".insert-emoji").classList.remove("open");
        document.querySelector(".insert-emoji").classList.add("close");
      } else {
        document.querySelector(".insert-emoji").classList.remove("close");
        document.querySelector(".insert-emoji").classList.add("open");
      }
    }
    document.querySelector("#write-message-send").onclick = () => sendAction();
    document.querySelector("#write-message-input").addEventListener("keydown", function(e) {
      if(e.key === 'Enter') sendAction();
    });
  }

  updateChannels = () => {
    document.querySelector('#channel-list').innerHTML = "";
    if(Object.keys(channels).length > 0) {
      Object.keys(channels).forEach(channel => {
        const channelItem = document.createElement("LI");
        channelItem.dataset.channel = channel;
        if(channel === localStorage.getItem('activeChannel')) {
          channelItem.classList.add('active');
        }
        channelItem.innerHTML = "📡 " + channel;
        channelItem.style.cursor = "pointer";
        channelItem.onclick = function() {
          selectChannel(this.dataset.channel);
        }

        document.querySelector('#channel-list').appendChild(channelItem);
      });
    }
  }

  updateMessages = () => {
    document.querySelector('#msgs').innerHTML = "";
    if(messages[localStorage.getItem('activeChannel')] !== undefined) {
      channelMessages = messages[localStorage.getItem('activeChannel')]['messages'];
      channelMessages.forEach(message => {
        const messageItem = document.createElement("li");
        const messageTime = new Date(message['timestamp']);
        messageItem.innerHTML = message['nickname'] + " [" + messageTime.toLocaleString() + "]: " + unescape(message['message_text']);
        if(message['nickname'] === localStorage.getItem('nickname')) {
          messageItem.dataset.timestamp = message['timestamp'];
          messageItem.innerHTML += "<span class=\"del-msg\">×</span>";
          messageItem.classList.add('me');
        }

        document.querySelector('#msgs').appendChild(messageItem);
      });

      document.querySelectorAll('.del-msg').forEach(el => {
        el.onclick = () => {
          socket.emit('message delete', {
            'channel': localStorage.getItem('activeChannel'),
            'nickname': localStorage.getItem('nickname'),
            'timestamp': el.parentElement.dataset.timestamp
          });
        }
      });
      document.querySelector('#msgs').scrollTo(0, document.querySelector('#msgs').scrollHeight);
    }
  }

  showButtons = () => {
    if(document.querySelector('.add-channel') === null && localStorage.getItem('mychannel') === null) {
      const channelButton = document.createElement("BUTTON");
      channelButton.innerHTML = "📡 Create channel";
      channelButton.classList.add("btn");
      channelButton.classList.add("btn-warning");
      channelButton.classList.add("add-channel");
      document.querySelector('.channel-header').appendChild(channelButton);
      document.querySelector('.add-channel').onclick = () => {
        showDialog("📡 New channel", "Channel name", "Create", true, newChannelAction);
      }
    } else {
      if(document.querySelector('.add-channel')) {
        document.querySelector('.add-channel').style.display = "none";
      }
    }
  }

  selectChannel = (channel) => {
    if(localStorage.getItem('activeChannel') !== null) {
      document.querySelector(`[data-channel="${localStorage.getItem('activeChannel')}"]`).classList.remove('active');
    }
    localStorage.setItem('activeChannel', channel);
    document.querySelector(`[data-channel='${channel}']`).classList.add('active');
    updateMessages();
    document.querySelector("#messages-bg-text").innerHTML = "📡 " + localStorage.getItem('activeChannel');
    document.querySelector("#write-message-input").disabled = false;
    document.querySelector(".emoji").disabled = false;
    document.querySelector(".send").disabled = false;
    document.querySelector("#write-message-send").onclick = () => sendAction();
    document.querySelector("#write-message-input").addEventListener("keydown", function(e) {
      if(e.key === 'Enter') sendAction();
    });
  }

  hideModal = () => {
    document.querySelector('.backdrop').style.display = "none";
    document.querySelector('#modal').style.display = "none";
    document.querySelector('#modal').innerHTML = "";
  }

  hideButtons = () => {
    document.querySelector(".channel-header").removeChild(document.querySelector(".add-channel"));
  }

  showDialog = (title, placeholder, buttonText, canClose, onclickAction) => {
    document.querySelector('.backdrop').style.display = "flex";
    let modalContent = `<div class="modal-header"><h5 class="modal-title">${title}</h5>`;

    if(canClose === true) {
      modalContent += `<button type="button" class="close" id="modal-close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>`;
    }
    modalContent += `</div><div class="modal-body"><div class="form-group">
    <input id="modal-input" type="text" placeholder="${placeholder}." autofocus>
    <div id="modal-error" class="alert alert-danger" role="alert" style="display: none"></div>
    </div><button id="modal-btn" class="btn btn-primary">${buttonText}</button></div>`;

    document.querySelector('#modal').innerHTML = modalContent;
    document.querySelector('#modal-input').focus();
    document.querySelector("#modal-input").onkeydown = (e) => {
      if(e.key === 'Enter') onclickAction();
    };

    if(canClose === true) {
      document.querySelector('#modal-close').onclick = () => {
        document.querySelector('#modal-input').value = "";
        hideModal();
      }
    }

    document.querySelector('#modal-btn').onclick = onclickAction;
    document.querySelector('#modal').style.display = "block";

    document.querySelector("#modal-input").onkeydown = () => {
      document.querySelector("#modal-error").style.display = "none";
      document.querySelector("#modal-error").innerHTML = "";
    }
  }
});
