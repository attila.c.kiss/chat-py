import os

from flask import Flask, render_template, json
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
socketio = SocketIO(app)

channels = {}
users = []
messages = {}

@app.route("/")
def index():
    return render_template("index.html", users = users, channels = channels, messages = messages)

@socketio.on("user login")
def userLogin(data):
    if data['nickname'] not in users:
        users.append(data['nickname'])
    emit("users", users, broadcast=True)

@socketio.on("user logout")
def userLogout(data):
    users.remove(data['nickname'])
    emit("users", users, broadcast=True)

@socketio.on("new channel")
def channelNew(data):
    if data['channel'] not in channels:
        ch_key = data['channel']
        ch_value = data['owner']
        channels.update({ch_key: ch_value})
    emit("channels", channels, broadcast=True)

@socketio.on("message sent")
def messageSent(data):
    if data['channel'] not in messages.keys():
        messages[data['channel']] = {}

    if 'messages' not in messages[data['channel']].keys():
        messages[data['channel']]['messages'] = []
    if len(messages[data['channel']]['messages']) > 99:
        messages[data['channel']]['messages'].pop(0)
    messages[data['channel']]['messages'].append(data['message'])
    emit("messages", messages, broadcast=True)

@socketio.on("message delete")
def messageDelete(data):
    for msg in messages[data['channel']]['messages']:
        if msg['nickname'] == data['nickname'] and int(msg['timestamp']) == int(data['timestamp']):
            messages[data['channel']]['messages'].remove(msg)
    emit("messages", messages, broadcast=True)

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
